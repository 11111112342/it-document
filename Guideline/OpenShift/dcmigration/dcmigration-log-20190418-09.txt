login as: root
root@192.168.225.120's password:
Last login: Thu Apr 18 02:19:50 2019 from 192.168.225.253
[root@appnode1 ~]# oc login https://admin.ocp.eabdc:8443 --token=ZgCRe7Qg2lojQPB                                                                             3Evlo0TOqWW3bSC08We1KwwMpE68
Logged into "https://admin.ocp.eabdc:8443" as "admin" using the token provided.

You have access to the following projects and can switch between them with 'oc p                                                                             roject <projectname>':

    121demo
    ax-project-001
    axasgprod
    axasgsit
    axasgsit2
  * axasgsit3
    default
    demo2
    eab
    eab-121-v3-dev
    eab-cafe-dev
    eab-cafe-sit
    eab-ocr-service
    eab-time-sheet
    eab-time-sheet-dev
    eabtimesheet
    kube-public
    kube-service-catalog
    kube-system
    logging
    management-infra
    ocr-service-dev
    ocr-service-sit
    openshift
    openshift-infra
    openshift-metrics
    openshift-node
    openshift-web-console
    php
    sftp
    test

Using project "axasgsit3".
[root@appnode1 ~]# oc get nodes
NAME                   STATUS                        ROLES     AGE       VERSION
appnode1.ocp.eabdc     NotReady,SchedulingDisabled   compute   327d      v1.9.1+                                                                             a0ce1bc657
appnode2.ocp.eabdc     Ready                         compute   327d      v1.9.1+                                                                             a0ce1bc657
appnode3.ocp.eabdc     Ready                         compute   327d      v1.9.1+                                                                             a0ce1bc657
infranode1.ocp.eabdc   Ready                         infra     327d      v1.9.1+                                                                             a0ce1bc657
infranode2.ocp.eabdc   Ready                         infra     327d      v1.9.1+                                                                             a0ce1bc657
infranode3.ocp.eabdc   Ready                         infra     327d      v1.9.1+                                                                             a0ce1bc657
mnode1.ocp.eabdc       Ready                         master    327d      v1.9.1+                                                                             a0ce1bc657
mnode2.ocp.eabdc       Ready                         master    327d      v1.9.1+                                                                             a0ce1bc657
mnode3.ocp.eabdc       Ready                         master    327d      v1.9.1+                                                                             a0ce1bc657
[root@appnode1 ~]# systemctl stop docker atomic-openshift-node
[root@appnode1 ~]# rm -rf /var/lib/origin/openshift.local.volumes
[root@appnode1 ~]# rm -rf /var/lib/docker
[root@appnode1 ~]# docker-storage-setup --reset
  Logical volume "docker-pool" successfully removed
[root@appnode1 ~]# docker-storage-setup
INFO: Device /dev/sdb is already partitioned and is part of volume group docker-                                                                             vg
  Rounding up size to full physical extent 52.00 MiB
  Thin pool volume with chunk size 512.00 KiB can address at most 126.50 TiB of                                                                              data.
  Logical volume "docker-pool" created.
  Logical volume docker-vg/docker-pool changed.
[root@appnode1 ~]# mkdir /var/lib/docker
[root@appnode1 ~]# systemctl start docker atomic-openshift-node
[root@appnode1 ~]# oc adm manage-node appnode1.ocp.eabdc --schedulable=true
NAME                 STATUS    ROLES     AGE       VERSION
appnode1.ocp.eabdc   Ready     compute   327d      v1.9.1+a0ce1bc657
[root@appnode1 ~]# oc get nodes
NAME                   STATUS    ROLES     AGE       VERSION
appnode1.ocp.eabdc     Ready     compute   327d      v1.9.1+a0ce1bc657
appnode2.ocp.eabdc     Ready     compute   327d      v1.9.1+a0ce1bc657
appnode3.ocp.eabdc     Ready     compute   327d      v1.9.1+a0ce1bc657
infranode1.ocp.eabdc   Ready     infra     327d      v1.9.1+a0ce1bc657
infranode2.ocp.eabdc   Ready     infra     327d      v1.9.1+a0ce1bc657
infranode3.ocp.eabdc   Ready     infra     327d      v1.9.1+a0ce1bc657
mnode1.ocp.eabdc       Ready     master    327d      v1.9.1+a0ce1bc657
mnode2.ocp.eabdc       Ready     master    327d      v1.9.1+a0ce1bc657
mnode3.ocp.eabdc       Ready     master    327d      v1.9.1+a0ce1bc657
[root@appnode1 ~]# oc get node --show-labels
NAME                   STATUS    ROLES     AGE       VERSION             LABELS
appnode1.ocp.eabdc     Ready     compute   327d      v1.9.1+a0ce1bc657   beta.ku                                                                             bernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/hostname=appnod                                                                             e1.ocp.eabdc,logging-infra-fluentd=true,node-role.kubernetes.io/compute=true,reg                                                                             ion=primary,zone=default
appnode2.ocp.eabdc     Ready     compute   327d      v1.9.1+a0ce1bc657   beta.ku                                                                             bernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/hostname=appnod                                                                             e2.ocp.eabdc,logging-infra-fluentd=true,node-role.kubernetes.io/compute=true,reg                                                                             ion=primary,zone=default
appnode3.ocp.eabdc     Ready     compute   327d      v1.9.1+a0ce1bc657   beta.ku                                                                             bernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/hostname=appnod                                                                             e3.ocp.eabdc,logging-infra-fluentd=true,node-role.kubernetes.io/compute=true,reg                                                                             ion=primary,zone=default
infranode1.ocp.eabdc   Ready     infra     327d      v1.9.1+a0ce1bc657   beta.ku                                                                             bernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/hostname=infran                                                                             ode1.ocp.eabdc,logging-infra-fluentd=true,node-role.kubernetes.io/infra=true,reg                                                                             ion=infra,zone=east
infranode2.ocp.eabdc   Ready     infra     327d      v1.9.1+a0ce1bc657   beta.ku                                                                             bernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/hostname=infran                                                                             ode2.ocp.eabdc,logging-infra-fluentd=true,node-role.kubernetes.io/infra=true,reg                                                                             ion=infra,zone=west
infranode3.ocp.eabdc   Ready     infra     327d      v1.9.1+a0ce1bc657   beta.ku                                                                             bernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/hostname=infran                                                                             ode3.ocp.eabdc,logging-infra-fluentd=true,node-role.kubernetes.io/infra=true,reg                                                                             ion=infra,zone=north
mnode1.ocp.eabdc       Ready     master    327d      v1.9.1+a0ce1bc657   beta.ku                                                                             bernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/hostname=mnode1                                                                             .ocp.eabdc,logging-infra-fluentd=true,node-role.kubernetes.io/master=true,opensh                                                                             ift-infra=apiserver
mnode2.ocp.eabdc       Ready     master    327d      v1.9.1+a0ce1bc657   beta.ku                                                                             bernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/hostname=mnode2                                                                             .ocp.eabdc,logging-infra-fluentd=true,node-role.kubernetes.io/master=true
mnode3.ocp.eabdc       Ready     master    327d      v1.9.1+a0ce1bc657   beta.ku                                                                             bernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/hostname=mnode3                                                                             .ocp.eabdc,logging-infra-fluentd=true,node-role.kubernetes.io/master=true
[root@appnode1 ~]# oc get nodes
NAME                   STATUS    ROLES     AGE       VERSION
appnode1.ocp.eabdc     Ready     compute   327d      v1.9.1+a0ce1bc657
appnode2.ocp.eabdc     Ready     compute   327d      v1.9.1+a0ce1bc657
appnode3.ocp.eabdc     Ready     compute   327d      v1.9.1+a0ce1bc657
infranode1.ocp.eabdc   Ready     infra     327d      v1.9.1+a0ce1bc657
infranode2.ocp.eabdc   Ready     infra     327d      v1.9.1+a0ce1bc657
infranode3.ocp.eabdc   Ready     infra     327d      v1.9.1+a0ce1bc657
mnode1.ocp.eabdc       Ready     master    327d      v1.9.1+a0ce1bc657
mnode2.ocp.eabdc       Ready     master    327d      v1.9.1+a0ce1bc657
mnode3.ocp.eabdc       Ready     master    327d      v1.9.1+a0ce1bc657
[root@appnode1 ~]# oc get nodes --show-labels
NAME                   STATUS    ROLES     AGE       VERSION             LABELS
appnode1.ocp.eabdc     Ready     compute   327d      v1.9.1+a0ce1bc657   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/hostname=appnode1.ocp.eabdc,logging-infra-fluentd=true,node-role.kubernetes.io/compute=true,region=primary,zone=default
appnode2.ocp.eabdc     Ready     compute   327d      v1.9.1+a0ce1bc657   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/hostname=appnode2.ocp.eabdc,logging-infra-fluentd=true,node-role.kubernetes.io/compute=true,region=primary,zone=default
appnode3.ocp.eabdc     Ready     compute   327d      v1.9.1+a0ce1bc657   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/hostname=appnode3.ocp.eabdc,logging-infra-fluentd=true,node-role.kubernetes.io/compute=true,region=primary,zone=default
infranode1.ocp.eabdc   Ready     infra     327d      v1.9.1+a0ce1bc657   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/hostname=infranode1.ocp.eabdc,logging-infra-fluentd=true,node-role.kubernetes.io/infra=true,region=infra,zone=east
infranode2.ocp.eabdc   Ready     infra     327d      v1.9.1+a0ce1bc657   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/hostname=infranode2.ocp.eabdc,logging-infra-fluentd=true,node-role.kubernetes.io/infra=true,region=infra,zone=west
infranode3.ocp.eabdc   Ready     infra     327d      v1.9.1+a0ce1bc657   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/hostname=infranode3.ocp.eabdc,logging-infra-fluentd=true,node-role.kubernetes.io/infra=true,region=infra,zone=north
mnode1.ocp.eabdc       Ready     master    327d      v1.9.1+a0ce1bc657   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/hostname=mnode1.ocp.eabdc,logging-infra-fluentd=true,node-role.kubernetes.io/master=true,openshift-infra=apiserver
mnode2.ocp.eabdc       Ready     master    327d      v1.9.1+a0ce1bc657   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/hostname=mnode2.ocp.eabdc,logging-infra-fluentd=true,node-role.kubernetes.io/master=true
mnode3.ocp.eabdc       Ready     master    327d      v1.9.1+a0ce1bc657   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/hostname=mnode3.ocp.eabdc,logging-infra-fluentd=true,node-role.kubernetes.io/master=true
[root@appnode1 ~]#
