login as: root
root@192.168.225.115's password:
Last login: Mon Mar 18 22:34:24 2019 from 192.168.225.253
[root@mnode2 ~]# cd /etc/origin
[root@mnode2 origin]# cd master
[root@mnode2 master]# ls
admin.crt                          master.etcd-client.crt
admin.key                          master.etcd-client.csr
admin.kubeconfig                   master.etcd-client.key
aggregator-front-proxy.crt         master.kubelet-client.crt
aggregator-front-proxy.key         master.kubelet-client.key
aggregator-front-proxy.kubeconfig  master.proxy-client.crt
bindPassword.encrypted             master.proxy-client.key
bindPassword.key                   master.server.crt
ca-bundle.crt                      master.server.key
ca.crt                             named_certificates
ca.key                             openshift-master.crt
client-ca-bundle.crt               openshift-master.key
front-proxy-ca.crt                 openshift-master.kubeconfig
front-proxy-ca.key                 policy.json
htpasswd                           scheduler.json
master-config.yaml                 serviceaccounts.private.key
master-config.yaml.20180604        serviceaccounts.public.key
master-config.yaml.20190226        service-signer.crt
master-config.yaml.orig            service-signer.key
master.etcd-ca.crt                 session-secrets.yaml
[root@mnode2 master]# cp master-config.yaml master-config.yaml.20190319
[root@mnode2 master]# vi master-config.yaml
[root@mnode2 master]# systemctl restart atomic-openshift-master-api atomic-openshift-master-controllers
[root@mnode2 master]# pwd
/etc/origin/master
[root@mnode2 master]# vi master-config.yaml
[root@mnode2 master]# systemctl restart atomic-openshift-master-api atomic-opens
[root@mnode2 master]# vi master-config.yaml                                     [root@mnode2 master]# systemctl restart atomic-openshift-master-api atomic-openshift-master-controllers
[root@mnode2 master]#
