println "SignDoc Web configuration - START"

sdweb.external_server_url="https://ease-sit3.intraeab/sdweb"

//################################################################################
//# custom settings                                                              #
//################################################################################

// uncommenting the next 2 lines below enables a simple prepare demo plugin
// that auto-inserts a signature filed in the lower left corner and
// pre-fills all text fields of the pdf document with the current date.
// source file: sdweb-samples-plugin*-soruces.zip/src/main/java/de/softpro/sdweb/plugins/impl/demo/PreparePlugin.java
//sdweb.plugins.loadlist=['de.softpro.sdweb.plugins.impl.demo.PreparePlugin']
//sdweb.defaults.prepare.plugin.impl='de.softpro.sdweb.plugins.impl.demo.PreparePlugin'

sdweb.plugins.loadlist = ['de.softpro.sdweb.plugins.impl.ServletDms']
//sdweb.defaults.prepare.plugin.impl='de.softpro.sdweb.plugins.impl.ServletDms'
sdweb.signature.display.signer=true
sdweb.defaults.signature.displaytext=" "

sdwebplugins.de.softpro.sdweb.plugins.impl.ServletDms.target_urls=["http://ease-web:3000/setAttachmentSigned"]
sdwebplugins.de.softpro.sdweb.plugins.impl.ServletDms.use_all_targets=true
sdwebplugins.de.softpro.sdweb.plugins.impl.ServletDms.createXMLFile=false

sdweb.authenticate.pluginid="de.softpro.sdweb.plugins.impl.BasicAuthenticator"
sdwebplugins.de.softpro.sdweb.plugins.impl.BasicAuthenticator.enabled=false

sdweb.certificate.store.pkcs12.file="/signdoc/signdoc_home/cert/cert_store.p12"
sdweb.certificate.store.pkcs12.password="secret"

sdweb.usage.enable.aboutpage=false
sdweb.usage.enable.loadpage=false

sdweb.signature.display.signtime=true
sdweb.defaults.signature.date.format="dd/MM/yyyy HH:mm:ss"

//############################################################################
//############################################################################
//### DO NOT CHANGE THE SETTINGS BELOW THIS LINE UNLESS ADVISED FROM KOFAX ###
//############################################################################
//############################################################################

// Signing client options
// sdweb.client.log.level = "SPEC_SIGCLT_LOGLEVEL"
sdweb.client.log.level = "ERROR"

// disable automatic UA detection
sdweb.mobile.remote_interface.detection.use_useragent=false

// allow multiple capture selections for unprepared signature fields
sdweb.digsig.unspecified.allow.subtype.choice=true

// log all requests including parameters on info level
sdweb.logging.all_requests=true

// exclude specific requested resource files (file extension which must be at the end of the URI) from request and response logging
sdweb.logging.exclude_uri_list_file_suffix=['.gif', '.png', '.jpg', '.css', '.js' ]
// exclude specific URIs (or parts from it) from request and response logging
// e.g. sdweb.logging.exclude_uri_list_request=['/LogService', '/ConfigurationService']
sdweb.logging.exclude_uri_list_request=[]

// log also body (if exists) of request (if the request is enabled for logging) on debug level
sdweb.logging.request_body=false
// output limit of the requested body log entry (in bytes), set -1 for unlimited
sdweb.logging.request_body_length=200

// log all response headers on debug level
sdweb.logging.response_headers=true
// log response content on debug level (if available)
sdweb.logging.response_content=false

// include specific URIs (or parts from it) for response content logging
// e.g. sdweb.logging.include_uri_list_response_content=['/services/', '/gwtrpc/']
sdweb.logging.include_uri_list_response_content=[]

// output limit of the response content log entry (in bytes), set -1 for unlimited
sdweb.logging.response_content_length=200



println "SignDoc Web configuration - END"