login as: root
root@192.168.225.167's password:
Welcome to Ubuntu 16.04.5 LTS (GNU/Linux 4.4.0-131-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

171 packages can be updated.
90 updates are security updates.


Last login: Tue Dec 11 10:51:38 2018 from 192.168.225.253
root@vm-kubernetes01:~# lscpu
Architecture:          x86_64
CPU op-mode(s):        32-bit, 64-bit
Byte Order:            Little Endian
CPU(s):                4
On-line CPU(s) list:   0-3
Thread(s) per core:    1
Core(s) per socket:    4
Socket(s):             1
NUMA node(s):          1
Vendor ID:             GenuineIntel
CPU family:            6
Model:                 79
Model name:            Intel(R) Xeon(R) CPU E5-2630 v4 @ 2.20GHz
Stepping:              1
CPU MHz:               2197.428
BogoMIPS:              4394.85
Hypervisor vendor:     Microsoft
Virtualization type:   full
L1d cache:             32K
L1i cache:             32K
L2 cache:              256K
L3 cache:              25600K
NUMA node0 CPU(s):     0-3
Flags:                 fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ss ht syscall nx pdpe1gb rdtscp lm constant_tsc rep_good nopl xtopology eagerfpu pni pclmulqdq ssse3 fma cx16 pcid sse4_1 sse4_2 movbe popcnt aes xsave avx f16c rdrand hypervisor lahf_lm abm 3dnowprefetch invpcid_single kaiser fsgsbase bmi1 hle avx2 smep bmi2 erms invpcid rtm rdseed adx smap xsaveopt
root@vm-kubernetes01:~# free
              total        used        free      shared  buff/cache   available
Mem:        8166220     1815664      307688       59716     6042868     5887944
Swap:             0           0           0
root@vm-kubernetes01:~# iotop
The program 'iotop' is currently not installed. You can install it by typing:
apt install iotop
root@vm-kubernetes01:~# top
top - 15:19:25 up 205 days, 22:50,  1 user,  load average: 1.22, 0.96, 0.60
Tasks: 175 total,   1 running, 174 sleeping,   0 stopped,   0 zombie
%Cpu(s):  0.0 us,  1.4 sy,  0.0 ni, 98.6 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
KiB Mem :  8166220 total,   301676 free,  1816348 used,  6048196 buff/cache
KiB Swap:        0 total,        0 free,        0 used.  5887132 avail Mem

  PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND
18577 root      20   0   41800   3628   3064 R   5.6  0.0   0:00.04 top
30625 root      20   0  142496  16492  10096 S   5.6  0.2 517:39.74 coredns
65224 root      20   0  207624 109356  50260 S   5.6  1.3   5710:24 kube-contr+
    1 root      20   0   37732   5712   3936 S   0.0  0.1   0:44.59 systemd
    2 root      20   0       0      0      0 S   0.0  0.0   0:00.47 kthreadd
    3 root      20   0       0      0      0 S   0.0  0.0   8:38.14 ksoftirqd/0
    5 root       0 -20       0      0      0 S   0.0  0.0   0:00.00 kworker/0:+
    7 root      20   0       0      0      0 S   0.0  0.0  77:42.15 rcu_sched
    8 root      20   0       0      0      0 S   0.0  0.0   0:00.00 rcu_bh
    9 root      rt   0       0      0      0 S   0.0  0.0   5:08.99 migration/0
   10 root      rt   0       0      0      0 S   0.0  0.0   0:31.91 watchdog/0
   11 root      rt   0       0      0      0 S   0.0  0.0   0:31.02 watchdog/1
   12 root      rt   0       0      0      0 S   0.0  0.0   5:09.51 migration/1
   13 root      20   0       0      0      0 S   0.0  0.0   8:28.96 ksoftirqd/1
   15 root       0 -20       0      0      0 S   0.0  0.0   0:00.00 kworker/1:+
   16 root      rt   0       0      0      0 S   0.0  0.0   0:30.77 watchdog/2
   17 root      rt   0       0      0      0 S   0.0  0.0   5:12.28 migration/2
root@vm-kubernetes01:~# yum install iotop
The program 'yum' is currently not installed. You can install it by typing:
apt install yum
root@vm-kubernetes01:~# apt install yum
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following packages were automatically installed and are no longer required:
  gyp javascript-common libjs-inherits libjs-jquery libjs-node-uuid
  libjs-underscore libssl-dev libssl-doc libuv1 libuv1-dev node-abbrev
  node-ansi node-ansi-color-table node-archy node-async node-block-stream
  node-combined-stream node-cookie-jar node-delayed-stream node-forever-agent
  node-form-data node-fstream node-fstream-ignore node-github-url-from-git
  node-glob node-graceful-fs node-gyp node-inherits node-ini
  node-json-stringify-safe node-lockfile node-lru-cache node-mime
  node-minimatch node-mkdirp node-mute-stream node-node-uuid node-nopt
  node-normalize-package-data node-npmlog node-once node-osenv node-qs
  node-read node-read-package-json node-request node-retry node-rimraf
  node-semver node-sha node-sigmund node-slide node-tar node-tunnel-agent
  node-underscore node-which python-pkg-resources zlib1g-dev
Use 'apt autoremove' to remove them.
The following additional packages will be installed:
  debugedit libarchive13 liblua5.2-0 libnspr4 libnss3 libnss3-nssdb
  libpython2.7 librpm3 librpmbuild3 librpmio3 librpmsign3 libsqlite0
  python-libxml2 python-lzma python-pycurl python-rpm python-sqlite
  python-sqlitecachec python-urlgrabber rpm rpm-common rpm2cpio
Suggested packages:
  rpm-i18n lrzip libcurl4-gnutls-dev python-pycurl-dbg python-pycurl-doc
  python-sqlite-dbg alien elfutils rpmlint rpm2html
The following NEW packages will be installed:
  debugedit libarchive13 liblua5.2-0 libnspr4 libnss3 libnss3-nssdb
  libpython2.7 librpm3 librpmbuild3 librpmio3 librpmsign3 libsqlite0
  python-libxml2 python-lzma python-pycurl python-rpm python-sqlite
  python-sqlitecachec python-urlgrabber rpm rpm-common rpm2cpio yum
0 upgraded, 23 newly installed, 0 to remove and 161 not upgraded.
Need to get 4,122 kB of archives.
After this operation, 16.4 MB of additional disk space will be used.
Do you want to continue? [Y/n] y
Get:1 http://hk.archive.ubuntu.com/ubuntu xenial/main amd64 liblua5.2-0 amd64 5.
Get:2 http://hk.archive.ubuntu.com/ubuntu xenial-updates/main amd64 libnspr4 amd
Get:3 http://hk.archive.ubuntu.com/ubuntu xenial-updates/main amd64 libnss3-nssd
Get:4 http://hk.archive.ubuntu.com/ubuntu xenial-updates/main amd64 libnss3 amd6
Get:5 http://hk.archive.ubuntu.com/ubuntu xenial/universe amd64 librpmio3 amd64
Get:6 http://hk.archive.ubuntu.com/ubuntu xenial/universe amd64 debugedit amd64
Get:7 http://hk.archive.ubuntu.com/ubuntu xenial-updates/main amd64 libarchive13
Get:8 http://hk.archive.ubuntu.com/ubuntu xenial-updates/main amd64 libpython2.7
Get:9 http://hk.archive.ubuntu.com/ubuntu xenial/universe amd64 librpm3 amd64 4.
Get:10 http://hk.archive.ubuntu.com/ubuntu xenial/universe amd64 librpmbuild3 am
Get:11 http://hk.archive.ubuntu.com/ubuntu xenial/universe amd64 librpmsign3 amd
Get:12 http://hk.archive.ubuntu.com/ubuntu xenial/universe amd64 libsqlite0 amd6
Get:13 http://hk.archive.ubuntu.com/ubuntu xenial-updates/main amd64 python-libx
Get:14 http://hk.archive.ubuntu.com/ubuntu xenial/universe amd64 python-lzma amd
Get:15 http://hk.archive.ubuntu.com/ubuntu xenial/universe amd64 python-sqlite a
Get:16 http://hk.archive.ubuntu.com/ubuntu xenial/main amd64 python-pycurl amd64
Get:17 http://hk.archive.ubuntu.com/ubuntu xenial/main amd64 python-urlgrabber a
Get:18 http://hk.archive.ubuntu.com/ubuntu xenial/universe amd64 rpm-common amd6
Get:19 http://hk.archive.ubuntu.com/ubuntu xenial/universe amd64 rpm2cpio amd64
Get:20 http://hk.archive.ubuntu.com/ubuntu xenial/universe amd64 rpm amd64 4.12.
Get:21 http://hk.archive.ubuntu.com/ubuntu xenial/universe amd64 python-rpm amd6
Get:22 http://hk.archive.ubuntu.com/ubuntu xenial/universe amd64 python-sqliteca
Get:23 http://hk.archive.ubuntu.com/ubuntu xenial/universe amd64 yum all 3.4.3-3
Fetched 4,122 kB in 3s (1,184 kB/s)
Selecting previously unselected package liblua5.2-0:amd64.
(Reading database ... 73163 files and directories currently installed.)
Preparing to unpack .../liblua5.2-0_5.2.4-1ubuntu1_amd64.deb ...
Unpacking liblua5.2-0:amd64 (5.2.4-1ubuntu1) ...
Selecting previously unselected package libnspr4:amd64.
Preparing to unpack .../libnspr4_2%3a4.13.1-0ubuntu0.16.04.1_amd64.deb ...
Unpacking libnspr4:amd64 (2:4.13.1-0ubuntu0.16.04.1) ...
Selecting previously unselected package libnss3-nssdb.
Preparing to unpack .../libnss3-nssdb_2%3a3.28.4-0ubuntu0.16.04.5_all.deb ...
Unpacking libnss3-nssdb (2:3.28.4-0ubuntu0.16.04.5) ...
Selecting previously unselected package libnss3:amd64.
Preparing to unpack .../libnss3_2%3a3.28.4-0ubuntu0.16.04.5_amd64.deb ...
Unpacking libnss3:amd64 (2:3.28.4-0ubuntu0.16.04.5) ...
Selecting previously unselected package librpmio3.
Preparing to unpack .../librpmio3_4.12.0.1+dfsg1-3build3_amd64.deb ...
Unpacking librpmio3 (4.12.0.1+dfsg1-3build3) ...
Selecting previously unselected package debugedit.
Preparing to unpack .../debugedit_4.12.0.1+dfsg1-3build3_amd64.deb ...
Unpacking debugedit (4.12.0.1+dfsg1-3build3) ...
Selecting previously unselected package libarchive13:amd64.
Preparing to unpack .../libarchive13_3.1.2-11ubuntu0.16.04.6_amd64.deb ...
Unpacking libarchive13:amd64 (3.1.2-11ubuntu0.16.04.6) ...
Selecting previously unselected package libpython2.7:amd64.
Preparing to unpack .../libpython2.7_2.7.12-1ubuntu0~16.04.4_amd64.deb ...
Unpacking libpython2.7:amd64 (2.7.12-1ubuntu0~16.04.4) ...
Selecting previously unselected package librpm3.
Preparing to unpack .../librpm3_4.12.0.1+dfsg1-3build3_amd64.deb ...
Unpacking librpm3 (4.12.0.1+dfsg1-3build3) ...
Selecting previously unselected package librpmbuild3.
Preparing to unpack .../librpmbuild3_4.12.0.1+dfsg1-3build3_amd64.deb ...
Unpacking librpmbuild3 (4.12.0.1+dfsg1-3build3) ...
Selecting previously unselected package librpmsign3.
Preparing to unpack .../librpmsign3_4.12.0.1+dfsg1-3build3_amd64.deb ...
Unpacking librpmsign3 (4.12.0.1+dfsg1-3build3) ...
Selecting previously unselected package libsqlite0.
Preparing to unpack .../libsqlite0_2.8.17-12fakesync1_amd64.deb ...
Unpacking libsqlite0 (2.8.17-12fakesync1) ...
Selecting previously unselected package python-libxml2.
Preparing to unpack .../python-libxml2_2.9.3+dfsg1-1ubuntu0.6_amd64.deb ...
Unpacking python-libxml2 (2.9.3+dfsg1-1ubuntu0.6) ...
Selecting previously unselected package python-lzma.
Preparing to unpack .../python-lzma_0.5.3-3_amd64.deb ...
Unpacking python-lzma (0.5.3-3) ...
Selecting previously unselected package python-sqlite.
Preparing to unpack .../python-sqlite_1.0.1-12_amd64.deb ...
Unpacking python-sqlite (1.0.1-12) ...
Selecting previously unselected package python-pycurl.
Preparing to unpack .../python-pycurl_7.43.0-1ubuntu1_amd64.deb ...
Unpacking python-pycurl (7.43.0-1ubuntu1) ...
Selecting previously unselected package python-urlgrabber.
Preparing to unpack .../python-urlgrabber_3.9.1-4.2ubuntu1_all.deb ...
Unpacking python-urlgrabber (3.9.1-4.2ubuntu1) ...
Selecting previously unselected package rpm-common.
Preparing to unpack .../rpm-common_4.12.0.1+dfsg1-3build3_amd64.deb ...
Unpacking rpm-common (4.12.0.1+dfsg1-3build3) ...
Selecting previously unselected package rpm2cpio.
Preparing to unpack .../rpm2cpio_4.12.0.1+dfsg1-3build3_amd64.deb ...
Unpacking rpm2cpio (4.12.0.1+dfsg1-3build3) ...
Selecting previously unselected package rpm.
Preparing to unpack .../rpm_4.12.0.1+dfsg1-3build3_amd64.deb ...
Unpacking rpm (4.12.0.1+dfsg1-3build3) ...
Selecting previously unselected package python-rpm.
Preparing to unpack .../python-rpm_4.12.0.1+dfsg1-3build3_amd64.deb ...
Unpacking python-rpm (4.12.0.1+dfsg1-3build3) ...
Selecting previously unselected package python-sqlitecachec.
Preparing to unpack .../python-sqlitecachec_1.1.4-1_amd64.deb ...
Unpacking python-sqlitecachec (1.1.4-1) ...
Selecting previously unselected package yum.
Preparing to unpack .../archives/yum_3.4.3-3_all.deb ...
Unpacking yum (3.4.3-3) ...
Processing triggers for libc-bin (2.23-0ubuntu10) ...
Processing triggers for man-db (2.7.5-1) ...
Setting up liblua5.2-0:amd64 (5.2.4-1ubuntu1) ...
Setting up libnspr4:amd64 (2:4.13.1-0ubuntu0.16.04.1) ...
Setting up libarchive13:amd64 (3.1.2-11ubuntu0.16.04.6) ...
Setting up libpython2.7:amd64 (2.7.12-1ubuntu0~16.04.4) ...
Setting up libsqlite0 (2.8.17-12fakesync1) ...
Setting up python-libxml2 (2.9.3+dfsg1-1ubuntu0.6) ...
Setting up python-lzma (0.5.3-3) ...
Setting up python-sqlite (1.0.1-12) ...
Setting up python-pycurl (7.43.0-1ubuntu1) ...
Setting up python-urlgrabber (3.9.1-4.2ubuntu1) ...
Setting up python-sqlitecachec (1.1.4-1) ...
Setting up libnss3-nssdb (2:3.28.4-0ubuntu0.16.04.5) ...
Setting up libnss3:amd64 (2:3.28.4-0ubuntu0.16.04.5) ...
Setting up librpmio3 (4.12.0.1+dfsg1-3build3) ...
Setting up debugedit (4.12.0.1+dfsg1-3build3) ...
Setting up librpm3 (4.12.0.1+dfsg1-3build3) ...
Setting up librpmbuild3 (4.12.0.1+dfsg1-3build3) ...
Setting up librpmsign3 (4.12.0.1+dfsg1-3build3) ...
Setting up rpm-common (4.12.0.1+dfsg1-3build3) ...
Setting up rpm2cpio (4.12.0.1+dfsg1-3build3) ...
Setting up rpm (4.12.0.1+dfsg1-3build3) ...
Setting up python-rpm (4.12.0.1+dfsg1-3build3) ...
Setting up yum (3.4.3-3) ...
Processing triggers for libc-bin (2.23-0ubuntu10) ...
root@vm-kubernetes01:~# yum install iotop
There are no enabled repos.
 Run "yum repolist all" to see the repos you have.
 You can enable repos with yum-config-manager --enable <repo>
root@vm-kubernetes01:~# yum repolist all
repolist: 0
root@vm-kubernetes01:~# apt-get install iotop
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following packages were automatically installed and are no longer required:
  gyp javascript-common libjs-inherits libjs-jquery libjs-node-uuid libjs-unders
  node-ansi-color-table node-archy node-async node-block-stream node-combined-st
  node-fstream node-fstream-ignore node-github-url-from-git node-glob node-grace
  node-lockfile node-lru-cache node-mime node-minimatch node-mkdirp node-mute-st
  node-once node-osenv node-qs node-read node-read-package-json node-request nod
  node-tunnel-agent node-underscore node-which python-pkg-resources zlib1g-dev
Use 'apt autoremove' to remove them.
The following NEW packages will be installed:
  iotop
0 upgraded, 1 newly installed, 0 to remove and 161 not upgraded.
Need to get 23.8 kB of archives.
After this operation, 127 kB of additional disk space will be used.
Get:1 http://hk.archive.ubuntu.com/ubuntu xenial/universe amd64 iotop amd64 0.6-
Fetched 23.8 kB in 0s (86.0 kB/s)
Selecting previously unselected package iotop.
(Reading database ... 73724 files and directories currently installed.)
Preparing to unpack .../archives/iotop_0.6-1_amd64.deb ...
Unpacking iotop (0.6-1) ...
Processing triggers for man-db (2.7.5-1) ...
Setting up iotop (0.6-1) ...
root@vm-kubernetes01:~# iotop
root@vm-kubernetes01:~# df -h
Filesystem                   Size  Used Avail Use% Mounted on
udev                         3.9G     0  3.9G   0% /dev
tmpfs                        798M   58M  740M   8% /run
/dev/mapper/ubuntu--vg-root  146G  9.3G  130G   7% /
tmpfs                        3.9G  516K  3.9G   1% /dev/shm
tmpfs                        5.0M     0  5.0M   0% /run/lock
tmpfs                        3.9G     0  3.9G   0% /sys/fs/cgroup
/dev/sda1                    720M   58M  625M   9% /boot
none                         146G  9.3G  130G   7% /var/lib/docker/aufs/mnt/b2ae
shm                           64M     0   64M   0% /var/lib/docker/containers/89
none                         146G  9.3G  130G   7% /var/lib/docker/aufs/mnt/98d2
none                         146G  9.3G  130G   7% /var/lib/docker/aufs/mnt/d9ba
none                         146G  9.3G  130G   7% /var/lib/docker/aufs/mnt/3e37
shm                           64M     0   64M   0% /var/lib/docker/containers/68
shm                           64M     0   64M   0% /var/lib/docker/containers/da
shm                           64M     0   64M   0% /var/lib/docker/containers/bd
none                         146G  9.3G  130G   7% /var/lib/docker/aufs/mnt/7f44
none                         146G  9.3G  130G   7% /var/lib/docker/aufs/mnt/3090
tmpfs                        3.9G   12K  3.9G   1% /var/lib/kubelet/pods/9af1447s2bw
tmpfs                        3.9G   12K  3.9G   1% /var/lib/kubelet/pods/9ae9484
none                         146G  9.3G  130G   7% /var/lib/docker/aufs/mnt/7d8f
shm                           64M     0   64M   0% /var/lib/docker/containers/96
none                         146G  9.3G  130G   7% /var/lib/docker/aufs/mnt/c4a8
shm                           64M     0   64M   0% /var/lib/docker/containers/90
none                         146G  9.3G  130G   7% /var/lib/docker/aufs/mnt/8dec
none                         146G  9.3G  130G   7% /var/lib/docker/aufs/mnt/c4d7
none                         146G  9.3G  130G   7% /var/lib/docker/aufs/mnt/cfdb
none                         146G  9.3G  130G   7% /var/lib/docker/aufs/mnt/4cac
tmpfs                        3.9G   12K  3.9G   1% /var/lib/kubelet/pods/9af7eb2q
tmpfs                        3.9G   12K  3.9G   1% /var/lib/kubelet/pods/9af16d3q
none                         146G  9.3G  130G   7% /var/lib/docker/aufs/mnt/bf4c
none                         146G  9.3G  130G   7% /var/lib/docker/aufs/mnt/4d9c
shm                           64M     0   64M   0% /var/lib/docker/containers/d1
shm                           64M     0   64M   0% /var/lib/docker/containers/84
none                         146G  9.3G  130G   7% /var/lib/docker/aufs/mnt/486d
none                         146G  9.3G  130G   7% /var/lib/docker/aufs/mnt/433e
none                         146G  9.3G  130G   7% /var/lib/docker/aufs/mnt/cb53
none                         146G  9.3G  130G   7% /var/lib/docker/aufs/mnt/bcb8
tmpfs                        798M     0  798M   0% /run/user/0
root@vm-kubernetes01:~# cd /etc
root@vm-kubernetes01:/etc# ls
acpi                    cron.hourly     grub.d           kernel-img.conf  mailca
adduser.conf            cron.monthly    gshadow          kubernetes       mailca
alternatives            crontab         gshadow-         ldap             manpat
apache2                 cron.weekly     gss              ld.so.cache      mdadm
apm                     crypttab        hdparm.conf      ld.so.conf       mime.t
apparmor                dbus-1          host.conf        ld.so.conf.d     mke2fs
apparmor.d              debconf.conf    hostname         legal            modpro
apport                  debian_version  hosts            libaudit.conf    module
apt                     default         hosts.allow      libnl-3          module
at.deny                 deluser.conf    hosts.deny       lighttpd         mtab
bash.bashrc             depmod.d        init             locale.alias     nanorc
bash_completion         dhcp            init.d           locale.gen       networ
bash_completion.d       docker          initramfs-tools  localtime        networ
bindresvport.blacklist  dpkg            inputrc          logcheck         newt
binfmt.d                environment     insserv          login.defs       nsswit
byobu                   ethertypes      insserv.conf     logrotate.conf   opt
ca-certificates         fonts           insserv.conf.d   logrotate.d      os-rel
ca-certificates.conf    fstab           iproute2         lsb-release      overla
calendar                fuse.conf       iscsi            ltrace.conf      pam.co
cni                     gai.conf        issue            lvm              pam.d
console-setup           groff           issue.net        machine-id       passwd
cron.d                  group           kbd              magic            passwd
cron.daily              group-          kernel           magic.mime       perl
root@vm-kubernetes01:/etc# cd sysconfig
-bash: cd: sysconfig: No such file or directory
root@vm-kubernetes01:/etc# cd docker
root@vm-kubernetes01:/etc/docker# ls
key.json
root@vm-kubernetes01:/etc/docker# cd ..
root@vm-kubernetes01:/etc# ls
acpi                    cron.hourly     grub.d           kernel-img.conf  mailca
adduser.conf            cron.monthly    gshadow          kubernetes       mailca
alternatives            crontab         gshadow-         ldap             manpat
apache2                 cron.weekly     gss              ld.so.cache      mdadm
apm                     crypttab        hdparm.conf      ld.so.conf       mime.t
apparmor                dbus-1          host.conf        ld.so.conf.d     mke2fs
apparmor.d              debconf.conf    hostname         legal            modpro
apport                  debian_version  hosts            libaudit.conf    module
apt                     default         hosts.allow      libnl-3          module
at.deny                 deluser.conf    hosts.deny       lighttpd         mtab
bash.bashrc             depmod.d        init             locale.alias     nanorc
bash_completion         dhcp            init.d           locale.gen       networ
bash_completion.d       docker          initramfs-tools  localtime        networ
bindresvport.blacklist  dpkg            inputrc          logcheck         newt
binfmt.d                environment     insserv          login.defs       nsswit
byobu                   ethertypes      insserv.conf     logrotate.conf   opt
ca-certificates         fonts           insserv.conf.d   logrotate.d      os-rel
ca-certificates.conf    fstab           iproute2         lsb-release      overla
calendar                fuse.conf       iscsi            ltrace.conf      pam.co
cni                     gai.conf        issue            lvm              pam.d
console-setup           groff           issue.net        machine-id       passwd
cron.d                  group           kbd              magic            passwd
cron.daily              group-          kernel           magic.mime       perl
root@vm-kubernetes01:/etc# ls
acpi                    insserv.conf             protocols
adduser.conf            insserv.conf.d           python
alternatives            iproute2                 python2.7
apache2                 iscsi                    python3
apm                     issue                    python3.5
apparmor                issue.net                rc0.d
apparmor.d              kbd                      rc1.d
apport                  kernel                   rc2.d
apt                     kernel-img.conf          rc3.d
at.deny                 kubernetes               rc4.d
bash.bashrc             ldap                     rc5.d
bash_completion         ld.so.cache              rc6.d
bash_completion.d       ld.so.conf               rc.local
bindresvport.blacklist  ld.so.conf.d             rcS.d
binfmt.d                legal                    resolvconf
byobu                   libaudit.conf            resolv.conf
ca-certificates         libnl-3                  rmt
ca-certificates.conf    lighttpd                 rpc
calendar                locale.alias             rsyslog.conf
cni                     locale.gen               rsyslog.d
console-setup           localtime                screenrc
cron.d                  logcheck                 securetty
cron.daily              login.defs               security
cron.hourly             logrotate.conf           selinux
cron.monthly            logrotate.d              services
crontab                 lsb-release              sgml
cron.weekly             ltrace.conf              shadow
crypttab                lvm                      shadow-
dbus-1                  machine-id               shells
debconf.conf            magic                    skel
debian_version          magic.mime               sos.conf
default                 mailcap                  ssh
deluser.conf            mailcap.order            ssl
depmod.d                manpath.config           subgid
dhcp                    mdadm                    subgid-
docker                  mime.types               subuid
dpkg                    mke2fs.conf              subuid-
environment             modprobe.d               sudoers
ethertypes              modules                  sudoers.d
fonts                   modules-load.d           sysctl.conf
fstab                   mtab                     sysctl.d
fuse.conf               nanorc                   systemd
gai.conf                network                  terminfo
groff                   networks                 timezone
group                   newt                     tmpfiles.d
group-                  nsswitch.conf            ucf.conf
grub.d                  opt                      udev
gshadow                 os-release               ufw
gshadow-                overlayroot.conf         updatedb.conf
gss                     pam.conf                 update-manager
hdparm.conf             pam.d                    update-motd.d
host.conf               passwd                   update-notifier
hostname                passwd-                  vim
hosts                   perl                     vmware-tools
hosts.allow             pki                      vtrgb
hosts.deny              pm                       wgetrc
init                    polkit-1                 X11
init.d                  popularity-contest.conf  xdg
initramfs-tools         ppp                      xml
inputrc                 profile                  yum
insserv                 profile.d                zsh_command_not_found
root@vm-kubernetes01:/etc# cd ..
root@vm-kubernetes01:/# ls
bin   etc         lib         media  proc  sbin  sys  var
boot  home        lib64       mnt    root  snap  tmp  vmlinuz
dev   initrd.img  lost+found  opt    run   srv   usr
root@vm-kubernetes01:/# cd /etc/hosts
-bash: cd: /etc/hosts: Not a directory
root@vm-kubernetes01:/# cd etc
root@vm-kubernetes01:/etc# ls
acpi                    insserv.conf             protocols
adduser.conf            insserv.conf.d           python
alternatives            iproute2                 python2.7
apache2                 iscsi                    python3
apm                     issue                    python3.5
apparmor                issue.net                rc0.d
apparmor.d              kbd                      rc1.d
apport                  kernel                   rc2.d
apt                     kernel-img.conf          rc3.d
at.deny                 kubernetes               rc4.d
bash.bashrc             ldap                     rc5.d
bash_completion         ld.so.cache              rc6.d
bash_completion.d       ld.so.conf               rc.local
bindresvport.blacklist  ld.so.conf.d             rcS.d
binfmt.d                legal                    resolvconf
byobu                   libaudit.conf            resolv.conf
ca-certificates         libnl-3                  rmt
ca-certificates.conf    lighttpd                 rpc
calendar                locale.alias             rsyslog.conf
cni                     locale.gen               rsyslog.d
console-setup           localtime                screenrc
cron.d                  logcheck                 securetty
cron.daily              login.defs               security
cron.hourly             logrotate.conf           selinux
cron.monthly            logrotate.d              services
crontab                 lsb-release              sgml
cron.weekly             ltrace.conf              shadow
crypttab                lvm                      shadow-
dbus-1                  machine-id               shells
debconf.conf            magic                    skel
debian_version          magic.mime               sos.conf
default                 mailcap                  ssh
deluser.conf            mailcap.order            ssl
depmod.d                manpath.config           subgid
dhcp                    mdadm                    subgid-
docker                  mime.types               subuid
dpkg                    mke2fs.conf              subuid-
environment             modprobe.d               sudoers
ethertypes              modules                  sudoers.d
fonts                   modules-load.d           sysctl.conf
fstab                   mtab                     sysctl.d
fuse.conf               nanorc                   systemd
gai.conf                network                  terminfo
groff                   networks                 timezone
group                   newt                     tmpfiles.d
group-                  nsswitch.conf            ucf.conf
grub.d                  opt                      udev
gshadow                 os-release               ufw
gshadow-                overlayroot.conf         updatedb.conf
gss                     pam.conf                 update-manager
hdparm.conf             pam.d                    update-motd.d
host.conf               passwd                   update-notifier
hostname                passwd-                  vim
hosts                   perl                     vmware-tools
hosts.allow             pki                      vtrgb
hosts.deny              pm                       wgetrc
init                    polkit-1                 X11
init.d                  popularity-contest.conf  xdg
initramfs-tools         ppp                      xml
inputrc                 profile                  yum
insserv                 profile.d                zsh_command_not_found
root@vm-kubernetes01:/etc# vi hosts
root@vm-kubernetes01:/etc# cd ..
root@vm-kubernetes01:/# ls
bin   etc         lib         media  proc  sbin  sys  var
boot  home        lib64       mnt    root  snap  tmp  vmlinuz
dev   initrd.img  lost+found  opt    run   srv   usr
root@vm-kubernetes01:/# kubectl get nodes
NAME              STATUS   ROLES    AGE    VERSION
vm-kubernetes01   Ready    master   205d   v1.13.0
vm-kubernetes02   Ready    <none>   205d   v1.13.0
vm-kubernetes03   Ready    <none>   205d   v1.13.0
root@vm-kubernetes01:/# kubectl get pods
NAME                          READY   STATUS    RESTARTS   AGE
helloworld-79b64976cd-sz247   1/1     Running   0          205d
root@vm-kubernetes01:/#
