login as: root
root@192.168.225.113's password:
Last login: Fri Mar  1 17:38:41 2019 from 192.168.225.253
[root@lb ~]# cd /nfs
[root@lb nfs]# cd /
[root@lb /]# ls
ax   boot  etc   lib    media  nfs  ose39rpms  repos  run   srv  test  usr
bin  dev   home  lib64  mnt    opt  proc       root   sbin  sys  tmp   var
[root@lb /]# cd nfs
[root@lb nfs]# ls
etcd        logging-es-ops  prometheus              prometheus-alertmanager
logging-es  metrics         prometheus-alertbuffer  registry
[root@lb nfs]# cd registry
[root@lb registry]# ls
docker
[root@lb registry]# cd docker
[root@lb docker]# ls
registry
[root@lb docker]# pwd
/nfs/registry/docker
[root@lb docker]# cd re*
[root@lb registry]# ls
v2
[root@lb registry]# cd v2
[root@lb v2]# ls
blobs  repositories
[root@lb v2]# cd rep*
[root@lb repositories]# ls
demo2  eab  openshift  php  sftp  test
[root@lb repositories]# cd openshift
[root@lb openshift]# ls
ease-web  openshift-nginx-s2i  php-oci8-s2i
[root@lb openshift]# ls -al
total 0
drwxrwxr-x. 5 1000000000 nfsnobody 69 Feb 21 18:55 .
drwxrwxr-x. 8 1000000000 nfsnobody 82 Dec  5 16:46 ..
drwxr-xr-x. 4       1001 nfsnobody 37 Feb 21 18:55 ease-web
drwxrwxr-x. 5 1000000000 nfsnobody 55 Jun 14  2018 openshift-nginx-s2i
drwxrwxr-x. 5 1000000000 nfsnobody 55 Jul 12  2018 php-oci8-s2i
[root@lb openshift]# pwd
/nfs/registry/docker/registry/v2/repositories/openshift
[root@lb openshift]# cd ../..
[root@lb v2]# ls
blobs  repositories
[root@lb v2]# cd ..
[root@lb registry]# ls
v2
[root@lb registry]# cd ..
[root@lb docker]# ls
registry
[root@lb docker]# pwd
/nfs/registry/docker
[root@lb docker]# cd ..
[root@lb registry]# ls -al
total 0
drwxrwxr-x.  3 nfsnobody  nfsnobody  20 May 26  2018 .
drwxrwxrwx. 10 root       root      168 May 26  2018 ..
drwxrwxr-x.  3 1000000000 nfsnobody  22 May 26  2018 docker
[root@lb registry]# pwd
/nfs/registry
[root@lb registry]# cd ..
[root@lb nfs]# pwd
/nfs
[root@lb nfs]# ls
etcd        logging-es-ops  prometheus              prometheus-alertmanager
logging-es  metrics         prometheus-alertbuffer  registry
[root@lb nfs]# ls -al
total 0
drwxrwxrwx. 10 root      root      168 May 26  2018 .
dr-xr-xr-x. 22 root      root      287 Feb 12 17:52 ..
drwxrwxrwx.  2 nfsnobody nfsnobody   6 May 26  2018 etcd
drwxrwxrwx.  5 nfsnobody nfsnobody  33 May 27  2018 logging-es
drwxrwxrwx.  2 nfsnobody nfsnobody   6 May 26  2018 logging-es-ops
drwxrwxrwx.  4 nfsnobody nfsnobody  84 May 26  2018 metrics
drwxrwxrwx.  2 nfsnobody nfsnobody   6 May 26  2018 prometheus
drwxrwxrwx.  2 nfsnobody nfsnobody   6 May 26  2018 prometheus-alertbuffer
drwxrwxrwx.  2 nfsnobody nfsnobody   6 May 26  2018 prometheus-alertmanager
drwxrwxr-x.  3 nfsnobody nfsnobody  20 May 26  2018 registry
[root@lb nfs]# pwd
/nfs
[root@lb nfs]# ls -al
total 0
drwxrwxrwx. 10 root      root      168 May 26  2018 .
dr-xr-xr-x. 22 root      root      287 Feb 12 17:52 ..
drwxrwxrwx.  2 nfsnobody nfsnobody   6 May 26  2018 etcd
drwxrwxrwx.  5 nfsnobody nfsnobody  33 May 27  2018 logging-es
drwxrwxrwx.  2 nfsnobody nfsnobody   6 May 26  2018 logging-es-ops
drwxrwxrwx.  4 nfsnobody nfsnobody  84 May 26  2018 metrics
drwxrwxrwx.  2 nfsnobody nfsnobody   6 May 26  2018 prometheus
drwxrwxrwx.  2 nfsnobody nfsnobody   6 May 26  2018 prometheus-alertbuffer
drwxrwxrwx.  2 nfsnobody nfsnobody   6 May 26  2018 prometheus-alertmanager
drwxrwxr-x.  3 nfsnobody nfsnobody  20 May 26  2018 registry
[root@lb nfs]# cd registry
[root@lb registry]# ls
docker
[root@lb registry]# ls -al
total 0
drwxrwxr-x.  3 nfsnobody  nfsnobody  20 May 26  2018 .
drwxrwxrwx. 10 root       root      168 May 26  2018 ..
drwxrwxr-x.  3 1000000000 nfsnobody  22 May 26  2018 docker
[root@lb registry]# cd docker
[root@lb docker]# ls
registry
[root@lb docker]# ls -al
total 0
drwxrwxr-x. 3 1000000000 nfsnobody 22 May 26  2018 .
drwxrwxr-x. 3 nfsnobody  nfsnobody 20 May 26  2018 ..
drwxrwxr-x. 3 1000000000 nfsnobody 16 May 26  2018 registry
[root@lb docker]# cd ../..
[root@lb nfs]# ls
etcd        logging-es-ops  prometheus              prometheus-alertmanager
logging-es  metrics         prometheus-alertbuffer  registry
[root@lb nfs]# ls -al
total 0
drwxrwxrwx. 10 root      root      168 May 26  2018 .
dr-xr-xr-x. 22 root      root      287 Feb 12 17:52 ..
drwxrwxrwx.  2 nfsnobody nfsnobody   6 May 26  2018 etcd
drwxrwxrwx.  5 nfsnobody nfsnobody  33 May 27  2018 logging-es
drwxrwxrwx.  2 nfsnobody nfsnobody   6 May 26  2018 logging-es-ops
drwxrwxrwx.  4 nfsnobody nfsnobody  84 May 26  2018 metrics
drwxrwxrwx.  2 nfsnobody nfsnobody   6 May 26  2018 prometheus
drwxrwxrwx.  2 nfsnobody nfsnobody   6 May 26  2018 prometheus-alertbuffer
drwxrwxrwx.  2 nfsnobody nfsnobody   6 May 26  2018 prometheus-alertmanager
drwxrwxr-x.  3 nfsnobody nfsnobody  20 May 26  2018 registry
[root@lb nfs]# chmod -R 2775 registry
[root@lb nfs]# cd registry
[root@lb registry]# ls -al
total 0
drwxrwsr-x.  3 nfsnobody  nfsnobody  20 May 26  2018 .
drwxrwxrwx. 10 root       root      168 May 26  2018 ..
drwxrwsr-x.  3 1000000000 nfsnobody  22 May 26  2018 docker
[root@lb registry]#
